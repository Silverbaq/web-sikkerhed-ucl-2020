from flask import Flask, render_template, request, redirect
from flask_wtf.csrf import CSRFProtect

app = Flask(__name__)
app.config['SECRET_KEY'] = 'læksadfjklæwer802uriejwl8uoihsdiofh'
csrf = CSRFProtect(app)

users = {
    'bob': 'winner',
    'rick': 'bacon'
}


@app.route('/createUser', methods=['POST'])
def create_user():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        users[username] = password
    return redirect('/')


@app.route('/search', methods=['POST', 'GET'])
def search():
    if request.method == 'POST':
        name = request.form['name']
    else:
        name = request.args.get('name')
    password = users.get(name)
    return render_template('search.html', search=name, result={name: password})


@app.route('/')
def hello_world():
    return render_template("index.html", users=users)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)