from flask import Flask, render_template, request, escape

app = Flask(__name__)


@app.route('/', methods=["GET", "POST"])
def hello_world():
    data = ''
    if request.method == "POST":
        data = escape(request.form['data'])
    return render_template("index.html", bacon=data)


if __name__ == '__main__':
    app.run()
