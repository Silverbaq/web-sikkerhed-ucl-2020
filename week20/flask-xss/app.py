from flask import Flask, render_template, request, escape

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def hello_world():
    data = ''
    if request.method == "POST":
        data = request.form['data']
    return render_template("index.html", data=data)


if __name__ == '__main__':
    app.run()
