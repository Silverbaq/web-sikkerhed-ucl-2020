from flask import Flask
from flask_httpauth import HTTPTokenAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

app = Flask(__name__)
app.config['SECRET_KEY'] = 'dlfkjdlkfjsdlkfj'
token_serializer = Serializer(app.config['SECRET_KEY'], expires_in=3600)

auth = HTTPTokenAuth('Bearer')

users = ["john", "susan"]
for user in users:
    token = token_serializer.dumps({'username': user}).decode('utf-8')
    print('*** token for {}: {}\n'.format(user, token))


@auth.verify_token
def verify_token(token):
    try:
        data = token_serializer.loads(token)
    except:
        return False
    if 'username' in data:
        return data['username']


@app.route('/')
@auth.login_required
def index():
    return "Hello, {}!".format(auth.username())


if __name__ == '__main__':
    app.run()
