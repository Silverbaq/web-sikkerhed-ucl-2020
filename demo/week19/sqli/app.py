from flask import Flask, request, jsonify
import sqlite3

app = Flask(__name__)


def query_db(user, pwd):
    conn = sqlite3.connect('db.db')
    c = conn.cursor()

    #query = "SELECT * FROM users WHERE username = '{}' AND password = '{}'".format(user, pwd)

    query = "SELECT * FROM users WHERE username = ? AND password = ?"
    c.execute(query, (user, pwd))
    result = c.fetchall()
    conn.close()
    return result


@app.route('/')
def hello_world():
    username = request.args.get('user')
    password = request.args.get('pass')
    json = jsonify(users=query_db(username, password))
    return json


if __name__ == '__main__':
    app.run()
