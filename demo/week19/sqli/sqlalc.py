from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.db'
db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(120))

    def to_json(self):
        return {
            'id': self.id,
            'username': self.username,
            'password': self.password
        }


@app.route('/')
def hello_world():
    username = request.args.get('user')
    password = request.args.get('pass')

    result = User.query.filter_by(username=username, password=password).all()

    json = jsonify(users=result.to_json())
    return json


if __name__ == '__main__':
    app.run()
