from flask import Flask, render_template, request, redirect
from flask_wtf.csrf import CSRFProtect

app = Flask(__name__)
app.config['SECRET_KEY'] = 'dlfjh sdlfja fkhsdlæ fha'
csrf = CSRFProtect(app)

users = {
    'bob': 'winner',
    'rick': 'bacon'
}


@app.route('/createUser', methods=['POST'])
def create_user():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        users[username] = password
    return redirect('/')


@app.route('/search')
def search():
    search = request.args.get('search')
    result = users.get(search)
    return render_template('search.html', search=search, result=result)


@app.route('/')
def index():
    return render_template('index.html', users=users)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
